package com.fht.repository;

import com.fht.entity.Admin;

public interface AdminRepository {
    public Admin login(String username,String password);
}
