package com.fht.repository;

import com.fht.entity.User;

public interface UserRepository {
    public User login(String username,String password);
}
