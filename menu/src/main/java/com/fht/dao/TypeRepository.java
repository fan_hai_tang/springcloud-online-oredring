package com.fht.dao;


import com.fht.entity.Type;

import java.util.List;

public interface TypeRepository {
    public List<Type> findAll();
}
